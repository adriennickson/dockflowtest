import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-vessel-discharge',
  templateUrl: './vessel-discharge.component.html',
  styleUrls: ['./vessel-discharge.component.css']
})
export class VesselDischargeComponent implements OnInit {

  @Input() discharge: any;
  constructor() { }

  ngOnInit() {
  }

}
