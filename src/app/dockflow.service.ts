import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Shipment } from './shipment';
import { Tradeflow } from './tradeflow';


@Injectable({ providedIn: 'root' })
export class DockflowService {

    private apiUrl = 'https://run.dockflow.com/api/';
    private tradeflowsUrl = this.apiUrl + 'tradeflows?withChildren=%5B%22shipments%22%5D';
    private shipmentsUrl = this.apiUrl + 'shipments';

    httpOptions = {
        headers: new HttpHeaders({ 
            'Content-Type': 'application/json',
            'Authorization': `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjA1Y2I0NmJiOWM4YjlmYjc5ZWIyYmU0ZjgxNjUzYzc3OGUzMWJhYzM2M2ZhMjQ3Y2E5Nzc3YmE5OGU1OThhZmQ3MTBlZGQzODAwZmVhZjZjIn0.eyJhdWQiOiIyNiIsImp0aSI6IjA1Y2I0NmJiOWM4YjlmYjc5ZWIyYmU0ZjgxNjUzYzc3OGUzMWJhYzM2M2ZhMjQ3Y2E5Nzc3YmE5OGU1OThhZmQ3MTBlZGQzODAwZmVhZjZjIiwiaWF0IjoxNTY4MDM1MjE5LCJuYmYiOjE1NjgwMzUyMTksImV4cCI6MTU5OTY1NzYxOSwic3ViIjoiMTM5NSIsInNjb3BlcyI6W119.KCn8z9MoAJxz4Rew0se__IXwFgEgXiNJEae5S70fhePwz3whUhl0GfYdXYJgdQMUxVZRylOJXOZzLs3kpbsP49RUuug8-i8GNlTX7m-QzKAvi1BaOJe93ckHHk0PMPCpbkPsqoAm-l2Xgx0YUGTlLlFT0DHFw4AuGsDD1sV3Ol1wFKGqBDLN3c2KH_xcoiAO7UjwFJfBAyY9aKnTFBycNrUNFPfGq_c8CkfIGXuWDNqV4c-epFRJ9HuHAtELJkSqkdyHXsIl0UAj6cU2WfM7hO4sVT4tyJYfO1_ANWNShEfBzn9TYRMywdoYOvsifTBW1fYPRkcTpQiEXma36cjveZf5ekO5v3-U6iEqx77_s-0hVSiRwasDZpNWXuqgKzkoQ6ZjfKARhkGTneI-yuButSN1PQVwSVMBCsDTl3r0mILoqAoZC_-nX8Z-tlT7zb_K6gR6PNxTzNwKhYSJVOOQG2strqR8OYVKF5QNpcp7WfuxzBvcz11XzKGeHmEXv_4nd0dtbsA2hxD20AsqSi1cuoTEYn5XDvpiLiiIki9fZV3b8K3laUIqUJRG7LPkiOc6ZKC_XY-xw9e_15O9RNuKYqlCc6TFwKAJKFDPbALJqWYgJJ5NQSXSNZBwkPmoMnqW2tpYiK8skxeGil2FrIwDd7MUWnpyI0T8IF7skmQggUE`
        })
    };

  constructor(private http: HttpClient) { }


  /** GET tradeflows from the server */
  getTradeflows (): Observable<Tradeflow[]> {
    return this.http.get<Tradeflow[]>(this.tradeflowsUrl, this.httpOptions)
      .pipe(
        tap(_ => this.log('fetched tradeflows')),
        catchError(this.handleError<Tradeflow[]>('getTradeflows', []))
      );
  }


  /** GET shipment by id. */
  getShipment(id: number): Observable<Shipment> {
    const url = `${this.shipmentsUrl}/${id}?scopes=%5B%22TimelineData%22%5D`;
    return this.http.get<Shipment>(url, this.httpOptions)
    .pipe(
      tap(_ => this.log(`fetched shipment id=${id}`)),
      catchError(this.handleError<Shipment>(`getShipment id=${id}`))
    );
  }



  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a ShipmentService message with the MessageService */
  private log(message: string) {
    console.log(`Service: ${message}`);
  }
}