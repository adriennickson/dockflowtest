import { Component } from '@angular/core';
import { DockflowService } from './dockflow.service';
import { Tradeflow } from './tradeflow';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'dockflowTest';
  tradeflows: Tradeflow[];
  status =  "loading";

  constructor(private dockflowService: DockflowService) { }

  ngOnInit() {
    this.getTradeflows();
  }

  getTradeflows(): void {
    this.dockflowService.getTradeflows()
    .subscribe(tradeflows => {
      this.tradeflows = tradeflows;
      this.status = "success";
    }, error => { this.status = "error"; } );
  }

}
