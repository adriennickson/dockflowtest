export class Tradeflow {
    id: number;
    uuid_id: number;
    notes: object;
    active: boolean;
    docky_principal_entity_id: object;
    created_at: object;
    updated_at: object;
    auto_close: object;
    lead_contact_person_id: object;
    entities: object;
    name: string;
    shipments: object;
}