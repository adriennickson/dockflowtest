import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VesselLoadingComponent } from './vessel-loading.component';

describe('VesselLoadingComponent', () => {
  let component: VesselLoadingComponent;
  let fixture: ComponentFixture<VesselLoadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VesselLoadingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VesselLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
