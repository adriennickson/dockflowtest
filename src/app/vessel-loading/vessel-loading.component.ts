import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-vessel-loading',
  templateUrl: './vessel-loading.component.html',
  styleUrls: ['./vessel-loading.component.css']
})
export class VesselLoadingComponent implements OnInit {

  @Input() loading: any;
  constructor() { }

  ngOnInit() {
  }

}
