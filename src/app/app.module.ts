import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ShipmentComponent } from './shipment/shipment.component';
import { VesselLoadingComponent } from './vessel-loading/vessel-loading.component';
import { VesselDischargeComponent } from './vessel-discharge/vessel-discharge.component';

@NgModule({
  declarations: [
    AppComponent,
    ShipmentComponent,
    VesselLoadingComponent,
    VesselDischargeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
