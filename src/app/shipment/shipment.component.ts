import { Component, OnInit, Input } from '@angular/core';
import { Shipment } from '../shipment';
import { DockflowService } from './../dockflow.service';

@Component({
  selector: 'app-shipment',
  templateUrl: './shipment.component.html',
  styleUrls: ['./shipment.component.css']
})
export class ShipmentComponent implements OnInit {

  @Input() shipment: Shipment;
  status =  "loading";

  constructor(private dockflowService: DockflowService) { 
  }

  ngOnInit() {
    this.getShipment(this.shipment.id);
  }

  getShipment(id): void {
    this.dockflowService.getShipment(id)
    .subscribe(shipment => {
      this.status = "success";
      this.shipment = shipment;
      if( window.screen.width < 960)
        setTimeout(function(){ window.scrollTo(0,document.body.scrollHeight); }, 10);
    }, error => { this.status = "error"; } );
  }

}
