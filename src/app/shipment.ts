export class Shipment {
    id: number;
    name: string;
    tradeflow_id: number;
    uuid_id: number;
    created_at: object;
    updated_at: object;
    stage: number;
    sea_shipments:any
}